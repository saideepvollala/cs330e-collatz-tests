#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, collatz_calculate, collatz_increments, Cache

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "2 20\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  2)
        self.assertEqual(j, 20)

    def test_read_3(self):
        s = "100 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  100)
        self.assertEqual(j, 1)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_2(self):
        v = collatz_eval(900, 2050)
        self.assertEqual(v, 182)

    def test_eval_3(self):
        v = collatz_eval(1000001, 1000100)
        self.assertEqual(v, 259)

    def test_eval_4(self):
        v = collatz_eval(500, 5000)
        self.assertEqual(v, 238)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 20, 1, 21)
        self.assertEqual(w.getvalue(), "20 1 21\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 97, 154, 122)
        self.assertEqual(w.getvalue(), "97 154 122\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("10 1\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "10 1 20\n")

    def test_solve_3(self):
        r = StringIO("987 10105\n1284 928736\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "987 10105 262\n1284 928736 525\n")

    #*****the rest of the tests are for my own functions (2)
    #functions: collatz_calculate, collatz_increments

    # ----
    # calculate
    # ----


    def test_calculate_1(self):
        i, j = 101, 293
        self.assertEqual(128, collatz_calculate(i,j))

    def test_calculate_2(self):
        i, j = 29, 53
        self.assertEqual(110, collatz_calculate(i,j))

    def test_calculate_3(self):
        i, j = 1, 104
        self.assertEqual(119, collatz_calculate(i,j))

    # ----
    # increments
    # ----
    
    def test_increments_1(self):
        test_cache = Cache()
        i, j = 1500, 60500
        self.assertEqual((2,60), collatz_increments(i,j,test_cache))

    def test_increments_2(self):
        test_cache = Cache()
        i, j = 2900, 100500
        self.assertEqual((3,100), collatz_increments(i,j,test_cache))

    def test_increments_3(self):
        test_cache = Cache()
        i, j = 23001, 78000
        self.assertEqual((23,78), collatz_increments(i,j,test_cache))
        
    

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
