#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
        
    def test_read_long_int(self):
        s = "101010101 201010101\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 101010101)
        self.assertEqual(j, 201010101)
        
    def test_read_same_int(self):
        s = "10 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 10)
        self.assertEqual(j, 10)
        
    def test_read_too_many_inputs(self):
        s = "10 10 10 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 10)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)
        
    def test_eval_reverse_(self):
        v = collatz_eval(210, 201)
        self.assertEqual(v, 89)
    
    def test_eval_same_input(self):
        v = collatz_eval(10, 10)
        self.assertEqual(v, 7)
        
    def test_eval_one(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)
        
    def test_eval_large_range(self):
        v = collatz_eval(1, 99999)
        self.assertEqual(v, 351)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")
        
    def test_print_same_ints(self):
        w = StringIO()
        collatz_print(w, 100, 100, 100)
        self.assertEqual(w.getvalue(), "100 100 100\n")
        
    def test_print_reverse(self):
        w = StringIO()
        collatz_print(w, 125, 200, 100)
        self.assertEqual(w.getvalue(), "125 200 100\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
            
    def test_solve_same_ints(self):
        r = StringIO("1 1\n100 100\n200 200\n1000 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n100 100 26\n200 200 27\n1000 1000 112\n")
        
    def test_solve_reverse_ints(self):
        r = StringIO("10 1\n200 100\n210 201\n1000 900\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "10 1 20\n200 100 125\n210 201 89\n1000 900 174\n")
            
    def test_solve_many_ints(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n1 10\n100 200\n201 210\n900 1000\n1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n1 10 20\n100 200 125\n201 210 89\n900 1000 174\n1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
        

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
